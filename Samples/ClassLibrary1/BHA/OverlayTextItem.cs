﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHA
{
    public class OverlayTextItem
    {
        public string TextValue { get; set; }
        public Font Font;
        public Brush Brush;
        public string Position; //top-right | top-left | bottom-right | bottom-left
        public StringFormat StringFormat;
    }
}
