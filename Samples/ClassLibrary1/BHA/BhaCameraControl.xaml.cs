﻿using Camera_NET;
using DirectShowLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BHA
{
    /// <summary>
    /// Interaction logic for BhaCameraControl.xaml
    /// </summary>
    public partial class BhaCameraControl : UserControl, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        CameraChoice _CameraChoice = new CameraChoice();

        public BhaCameraControl()
        {
            InitializeComponent();

            Application.Current.Exit += AppExitHandler;

            this.ZoomFactor = 1.0;

            //creating dispatcher to update the time every second
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += (o, args) =>
            {
                //CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                this.CurrentDate = DateTime.Now;
            };
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();

            //initializing some of the properties
            this.ParcelNumber = "123456";
            this.UserComment = "Other predefined symbols DEBUG and TRACE constants.";
            this.ShowOverlayText = true;
            this.ShowDate = true;
            this.Fontsize1 = 12;

            this.DateTimeFormats = new List<string>();
            this.DateTimeFormats.Add("MM/dd/yyyy hh:mm tt");
            this.DateTimeFormats.Add("MM/dd/yyyy ddd hh:mm tt");
            this.DateTimeFormats.Add("MM/dd/yyyy hh:mm:ss tt");
            this.DateTimeFormats.Add("MM/dd/yyyy HH:mm tt");
            this.DateTimeFormats.Add("MM/dd/yy");

            //default format is set to the first item in the DateTimeFormat list
            this.SelectedDateTimeFormat = DateTimeFormats[0];

            //Timezones
            this.DateTimeKinds = new List<DateTimeKind>();
            DateTimeKinds.Add(DateTimeKind.Local);
            DateTimeKinds.Add(DateTimeKind.Utc);

            this.SelectedDateTimeKind = DateTimeKind.Local;
        }

        

        private void AppExitHandler(object sender, ExitEventArgs e)
        {
            try
            {
                this.CameraControlWpf1.CameraControl.CloseCamera();
            }
            catch
            {
                //Do nothing, error silently
            }
        }

        private void UpdateOverlayBitmap(Bitmap bmp, bool disposeOld)
        {
            var temp = this.CameraControlWpf1.CameraControl.OverlayBitmap;
            this.CameraControlWpf1.CameraControl.OverlayBitmap = bmp;
            if (disposeOld)
            {
                temp.Dispose();
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (DesignerProperties.GetIsInDesignMode(this)) {
                return;
            }

            if (e.Property.Name == "CurrentDevice")
            {
                DsDevice newDevice = (DsDevice)e.NewValue;
                if (newDevice != null)
                {
                    var bestRes = this.GetBestResolution(newDevice);
                    this.CameraControlWpf1.CameraControl.SetCamera(newDevice.Mon, bestRes);
                    //CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                    //this.CameraControlWpf1.CameraControl.SetCamera(newDevice.Mon, null);
                }
                else
                {
                    this.CameraControlWpf1.CameraControl.SetCamera(null, null);
                }
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("AvailableResolutions"));
                }
                this.CurrentResolution = this.CameraControlWpf1.CameraControl.Resolution;
            }


            if (e.Property.Name == "CurrentResolution")
            {
                Resolution newResolution = (Resolution)e.NewValue;
                if (this.CameraControlWpf1.CameraControl.Resolution != newResolution)
                {
                    this.CameraControlWpf1.CameraControl.SetCamera(this.CurrentDevice.Mon, newResolution);
                    //CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                    this.ZoomFactor = 1.0;
                }
            }

            if (e.Property.Name == "ShowText")
            {
                //CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                CameraControlWpf1.CameraControl.MixerEnabled = true;
            }

            if (e.Property.Name == "ZoomFactor")
            {
                if (this.CameraControlWpf1.CameraControl.CameraCreated)
                {
                    double newValue = (double)e.NewValue;
                    var zoom = newValue;

                    var resolution = this.CameraControlWpf1.CameraControl.Resolution;

                    var w = (resolution.Width / zoom);
                    var h = (resolution.Height / zoom);
                    var x = (resolution.Width - w) / 2.0;
                    var y = (resolution.Height - h) / 2.0;

                    System.Drawing.Rectangle rect = new System.Drawing.Rectangle(
                          (int)(x),
                          (int)(y),
                          (int)(w),
                          (int)(h)
                      );

                    this.CameraControlWpf1.CameraControl.ZoomToRect(rect);
                }
            }

            if (e.Property.Name == "ShowOverlayText")
            {
                if (!ShowOverlayText)
                {
                    CameraControlWpf1.CameraControl.MixerEnabled = false;
                }
                else
                {
                    if (CameraControlWpf1.CameraControl.CameraCreated)
                        CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }

            if (e.Property.Name == "ParcelNumber")
            {
                this.ParcelNumber = (string)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("ParcelNumber"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }
            if (e.Property.Name == "UserComment")
            {
                this.UserComment = (string)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("UserComment"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }

            if (e.Property.Name == "CurrentDate")
            {
                this.CurrentDate = DateTime.Now;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("CurrentDate"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }
            if (e.Property.Name == "Fontsize1")
            {
                this.Fontsize1 = (float)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("Fontsize1"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }

            if (e.Property.Name == "ShowDate")
            {
                this.ShowDate = (bool)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("ShowDate"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }

            if (e.Property.Name == "DateTimeFormats")
            {
                this.DateTimeFormats = (List<string>)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("DateTimeFormats"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }

            if (e.Property.Name == "SelectedDateTimeFormat")
            {
                this.SelectedDateTimeFormat = (string)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedDateTimeFormat"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }

            if (e.Property.Name == "SelectedDateTimeKind")
            {
                this.SelectedDateTimeKind = (DateTimeKind)e.NewValue;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("SelectedDateTimeKind"));
                    CameraControlWpf1.CameraControl.OverlayBitmap = RenderOverlayBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!Util.DesignMode)
            //{
            _CameraChoice.UpdateDeviceList();
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs("AvailableDevices"));
            }
            this.CurrentDevice = AvailableDevices.FirstOrDefault();
            //}
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.CameraControlWpf1.CameraControl.CloseCamera();
            }
            catch
            {
                //Do nothing, error silently
            }
        }

        public List<DateTimeKind> DateTimeKinds { get; }

        public List<DsDevice> AvailableDevices
        {
            get { return _CameraChoice.Devices; }
        }

        public List<Resolution> AvailableResolutions
        {
            get
            {
                if (this.CurrentDevice != null)
                {
                    ResolutionList resolutions = Camera.GetResolutionList(this.CurrentDevice.Mon);
                    return resolutions;
                }
                else
                {
                    return null;
                }

            }
        }

        public static readonly DependencyProperty CurrentDeviceProperty = DependencyProperty.Register("CurrentDevice", typeof(DsDevice), typeof(BhaCameraControl));
        public DsDevice CurrentDevice
        {
            get
            {
                return (DsDevice)this.GetValue(CurrentDeviceProperty);
            }
            set
            {
                this.SetValue(CurrentDeviceProperty, value);
            }
        }

        public static readonly DependencyProperty CurrentResolutionProperty = DependencyProperty.Register("CurrentResolution", typeof(Resolution), typeof(BhaCameraControl));
        public Resolution CurrentResolution
        {
            get
            {
                return (Resolution)this.GetValue(CurrentResolutionProperty);
            }
            set
            {
                this.SetValue(CurrentResolutionProperty, value);
            }
        }

        public static readonly DependencyProperty ZoomFactorProperty = DependencyProperty.Register("ZoomFactor", typeof(double), typeof(BhaCameraControl));
        public double ZoomFactor
        {
            get
            {
                return (double)this.GetValue(ZoomFactorProperty);
            }
            set
            {
                this.SetValue(ZoomFactorProperty, value);
            }
        }

        public static readonly DependencyProperty ShowOverlayTextProperty = DependencyProperty.Register("ShowOverlayText", typeof(bool), typeof(BhaCameraControl));
        public bool ShowOverlayText
        {
            get
            {
                return (bool)this.GetValue(ShowOverlayTextProperty);
            }
            set
            {
                this.SetValue(ShowOverlayTextProperty, value);

            }
        }

        public static readonly DependencyProperty ParcelNumberProperty = DependencyProperty.Register("ParcelNumber", typeof(string), typeof(BhaCameraControl));
        public string ParcelNumber
        {
            get
            {
                return (string)this.GetValue(ParcelNumberProperty);
            }
            set
            {
                this.SetValue(ParcelNumberProperty, value);
            }
        }

        public static readonly DependencyProperty UserCommentProperty = DependencyProperty.Register("UserComment", typeof(string), typeof(BhaCameraControl));
        public string UserComment
        {
            get
            {
                return (string)this.GetValue(UserCommentProperty);
            }
            set
            {
                this.SetValue(UserCommentProperty, value);
            }
        }

        public static readonly DependencyProperty CurrentDateProperty = DependencyProperty.Register("CurrentDate", typeof(DateTime), typeof(BhaCameraControl));
        public DateTime CurrentDate
        {
            get
            {
                return (DateTime)this.GetValue(CurrentDateProperty);
            }
            set
            {
                this.SetValue(CurrentDateProperty, value);
            }
        }

        public static readonly DependencyProperty Fontsize1Property = DependencyProperty.Register("Fontsize1", typeof(float), typeof(BhaCameraControl));
        public float Fontsize1
        {
            get
            {
                return (float)this.GetValue(Fontsize1Property);
            }
            set
            {
                this.SetValue(Fontsize1Property, value);
            }
        }

        public static readonly DependencyProperty ShowDateProperty = DependencyProperty.Register("ShowDate", typeof(bool), typeof(BhaCameraControl));
        public bool ShowDate
        {
            get
            {
                return (bool)this.GetValue(ShowDateProperty);
            }
            set
            {
                this.SetValue(ShowDateProperty, value);
            }
        }

        public static readonly DependencyProperty DateTimeFormatsProperty = DependencyProperty.Register("DateTimeFormats", typeof(List<string>), typeof(BhaCameraControl));
        public List<string> DateTimeFormats
        {
            get
            {
                return (List<string>)this.GetValue(DateTimeFormatsProperty);
            }
            set
            {
                this.SetValue(DateTimeFormatsProperty, value);
            }
        }

        public static readonly DependencyProperty SelectedDateTimeFormatProperty = DependencyProperty.Register("SelectedDateTimeFormat", typeof(string), typeof(BhaCameraControl));
        public string SelectedDateTimeFormat
        {
            get
            {
                return (string)this.GetValue(SelectedDateTimeFormatProperty);
            }
            set
            {
                this.SetValue(SelectedDateTimeFormatProperty, value);
            }
        }

        public static readonly DependencyProperty SelectedDateTimeKindProperty = DependencyProperty.Register("SelectedDateTimeKind", typeof(DateTimeKind), typeof(BhaCameraControl));
        public DateTimeKind SelectedDateTimeKind
        {
            get
            {
                return (DateTimeKind)this.GetValue(SelectedDateTimeKindProperty);
            }
            set
            {
                this.SetValue(SelectedDateTimeKindProperty, value);
            }
        }


        public System.Drawing.Bitmap GetSnapshot()
        {
            return CameraControlWpf1.CameraControl.SnapshotOutputImage();
        }

        private Bitmap RenderOverlayBitmap(bool useAntiAlias)
        {

            // see GenerateColorKeyBitmap
            int w = CameraControlWpf1.CameraControl.Resolution.Width;
            int h = CameraControlWpf1.CameraControl.Resolution.Height;

            if (w <= 0 || h <= 0)
                return null;

            // Create RGB bitmap
            Bitmap bmp = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            //create graphics context
            Graphics g = Graphics.FromImage(bmp);

            // configure antialiased drawing or not
            if (useAntiAlias)
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.TextRenderingHint = TextRenderingHint.AntiAlias;
            }
            else
            {
                g.SmoothingMode = SmoothingMode.None;
                g.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;
            }

            // Clear the bitmap with the color key
            g.Clear(CameraControlWpf1.CameraControl.GDIColorKey);


            //NOTE: instead of current code, iterate over _OverlayItems collection and render.
            //this.Fontsize1 = (float)(h / 25.0);
            var f = this.Fontsize1;
            if (h <= 320)
            {
                f = (float)(h / 15.0);
            }
          
            //if fontsize becomes lessthan 8 ; minimum size is 8
            if (f < 8)
            {
                f = 8;
            }

           
            Font font = new Font("Tahoma", f);

            System.Drawing.Brush textColorKeyed = new SolidBrush(System.Drawing.Color.Yellow);
        
            var parcelNumberSize = g.MeasureString(this.ParcelNumber, font);
            
            //check ShowOverlayText true
            if (ShowOverlayText)
            {
                //bottom-right (date)
                var dateStr = DateTime.Now.ToString(this.SelectedDateTimeFormat); // by default it is local time
                if (this.SelectedDateTimeKind == DateTimeKind.Utc)
                    dateStr = DateTime.UtcNow.ToString(this.SelectedDateTimeFormat);

                //dateSize 
                var dateSize = g.MeasureString(dateStr, font);

                //if date is not fitting reduce the font size untill it fits
                while (dateSize.Width > w)
                {
                    f = f - 2;
                    font = new Font("Tahoma", f);
                    dateSize = g.MeasureString(dateStr, font);
                    parcelNumberSize = g.MeasureString(this.ParcelNumber, font);
                }
               
                //if Parcelnumber and date overlaps on eachother, move the ParcelNumber little up
                if (dateSize.Width + parcelNumberSize.Width > w)
                {
                    parcelNumberSize.Height = 2 * parcelNumberSize.Height;
                }

                //check if ShowDate is true
                if (this.ShowDate)
                {
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1- 1, h - dateSize.Height - 1);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1- 1, h - dateSize.Height);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1- 1, h - dateSize.Height + 1);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1 + 1, h - dateSize.Height - 1);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1 + 1, h - dateSize.Height);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1 + 1, h - dateSize.Height - 1);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1, h - dateSize.Height + 1);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Black, w - dateSize.Width -1, h - dateSize.Height - 1);
                    g.DrawString(dateStr, font, System.Drawing.Brushes.Yellow, w - dateSize.Width -1, h - dateSize.Height);
                }

                //bottom left(Parcel Number)

                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 3, h - parcelNumberSize.Height - 1);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 3, h - parcelNumberSize.Height);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 3, h - parcelNumberSize.Height + 1);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 5, h - parcelNumberSize.Height - 1);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 5, h - parcelNumberSize.Height);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 5, h - parcelNumberSize.Height + 1);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 4, h - parcelNumberSize.Height + 1);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Black, 4, h - parcelNumberSize.Height - 1);
                g.DrawString(this.ParcelNumber, font, System.Drawing.Brushes.Yellow, 4, h - parcelNumberSize.Height);
                //gp.AddString(this.ParcelNumber, font.FontFamily, 0, emsize, new System.Drawing.Point(4, (int)(h - dateSize.Height)), sf);



                //Autowrapping userComment  
                var str = AutoWrapUserCommentString(this.UserComment, g, font, w, h);
                //g.DrawString(str, font, textColorKeyed, 4, 0);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 3, -1);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 3, 0);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 3, 1);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 5, -1);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 5, 0);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 5, 1);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 4, 1);
                g.DrawString(str, font, System.Drawing.Brushes.Black, 4, -1);
                g.DrawString(str, font, System.Drawing.Brushes.Yellow, 4, 0);               
            }
          
            g.Dispose();

            //render date (bottom right)
            //render parcelnumber (bottom left)
            //render user comment (top left), use AutoWrapUserCommentString

            //export as bitmap
            bmp.Save("c:\\Temp\\text.bmp");

            return bmp;

        }


        private string AutoWrapUserCommentString(string userComment, Graphics g, Font font, float w, float h)
        {
            //insert new-line characters where needed to avoid overflow image output size
            if (userComment != null)
            {
                StringBuilder sb = new StringBuilder();

                var commentSize = g.MeasureString(userComment, font);

                string[] words = userComment.Split(' ');
                sb = new StringBuilder();
                float wordsSize = 0;

                if (commentSize.Width > w)
                {
                    foreach (string word in words)
                    {
                        var wordsz = g.MeasureString(word, font).Width;
                        wordsSize = wordsSize + wordsz;

                        if (wordsSize < w)
                        {
                            if (sb.Length == 0)
                            {
                                //for the first word there is no need to have space 
                                sb.AppendFormat("{0}", word);
                            }
                            else
                            {
                                //Adding space between eachword
                                sb.AppendFormat(" {0}", word);
                            }
                        }
                        else
                        {
                            wordsSize = 0;
                            sb.AppendFormat("{0}{1}", Environment.NewLine, word);
                            wordsSize = wordsSize + wordsz;
                        }
                    }
                }
                else
                {
                    return userComment;
                }
                return sb.ToString();
            }
            return null;
        }


        private Resolution GetBestResolution(DsDevice ds)
        {
            ResolutionList resolutions = Camera.GetResolutionList(ds.Mon);

            var maxDots = 0;
            var maxRes = (Resolution)null;

            foreach (var r in resolutions)
            {
                var dots = r.Height * r.Width;
                if (maxDots < dots)
                {
                    maxDots = dots;
                    maxRes = r;
                }
            }

            return maxRes;

        }      


    }





    static class Util
    {
        /// <summary>
        /// Contains true, if we are in design mode of Visual Studio
        /// </summary>
        static bool _designMode;

        /// <summary>
        /// Initializes an instance of Util class
        /// </summary>
        static Util()
        {
            // design mode is true if host process is: Visual Studio, 
            // Visual Studio Express Versions (C#, VB, C++) or SharpDevelop
            var designerHosts = new List<string>() {
            "devenv", "vcsexpress", "vbexpress", "vcexpress", "sharpdevelop" };

            using (var process = System.Diagnostics.Process.GetCurrentProcess())
            {
                var processName = process.ProcessName.ToLower();
                _designMode = designerHosts.Contains(processName);
            }
        }

        /// <summary>
        /// Gets true, if we are in design mode of Visual Studio etc..
        /// </summary>
        public static bool DesignMode
        {
            get
            {
                return _designMode;
            }
        }
    }

}
