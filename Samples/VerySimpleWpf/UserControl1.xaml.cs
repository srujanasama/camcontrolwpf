﻿using Camera_NET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VerySimpleWpf
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl, INotifyPropertyChanged
    {
        public UserControl1()
        {
            InitializeComponent();
            _CameraChoice.UpdateDeviceList();

            AvailableDevices = new string[_CameraChoice.Devices.Count];

            for (int i = 0; i < _CameraChoice.Devices.Count; i++)
            {
                AvailableDevices[i] = _CameraChoice.Devices[i].Name;
            }
            availableDevices.ItemsSource = AvailableDevices;
            this.CurrentZoomFactor = 1;

        }

        CameraChoice _CameraChoice = new CameraChoice();


        private void AppExitHandler(object sender, ExitEventArgs e) {

            try
            {
                this.cameraControl.CameraControl.CloseCamera();
            }
            catch 
            {
                //Do nothing, error silently
            }

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Camera choice

            Application.Current.Exit += AppExitHandler;


            // Get List of devices (cameras)
            _CameraChoice.UpdateDeviceList();

            // To get an example of camera and resolution change look at other code samples 
            if (_CameraChoice.Devices.Count > 0)
            {
                // Device moniker. It's like device id or handle.
                // Run first camera if we have one
                var camera_moniker = _CameraChoice.Devices[0].Mon;
                //availableDevices.ItemsSource = _CameraChoice.Devices;
                // Set selected camera to camera control with default resolution
                cameraControl.CameraControl.SetCamera(camera_moniker, null);
                this.CameraActive = true;
            }
        }

        public bool CameraActive { get; set; }

        private string _CurrentDevice;
        public string CurrentDevice
        {
            get
            {
                return _CurrentDevice;
            }
            set
            {
                _CurrentDevice = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("CurrentDevice"));
                }
            }
        }

        private string _CurrentResolution;
        public string CurrentResolution
        {
            get
            {
                return _CurrentResolution;
            }
            set
            {
                _CurrentResolution = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("CurrentResolution"));
                }
            }
        }

        private int _CurrentZoomFactor;
        public int CurrentZoomFactor
        {
            get
            {
                return _CurrentZoomFactor;
            }
            set
            {
                _CurrentZoomFactor = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("CurrentZoomFactor"));
                }
            }
        }


        public string[] AvailableDevices { get; }
        public string[] AvailableResolutions { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        public byte[] GetSnapShot()
        {
            return null;
        }


        private void close_Click(object sender, RoutedEventArgs e)
        {
            if (this.CameraActive)
            {
                cameraControl.CameraControl.CloseCamera();
                           
                this.CameraActive = false;
            }
        }

        private void FillResolutionList()
        {
            availableResolutions.Items.Clear();

            if (!cameraControl.CameraControl.CameraCreated)
                return;

            ResolutionList resolutions = Camera.GetResolutionList(cameraControl.CameraControl.Moniker);

            if (resolutions == null)
                return;


            int index_to_select = -1;

            for (int index = 0; index < resolutions.Count; index++)
            {
                availableResolutions.Items.Add(resolutions[index].ToString());

                if (resolutions[index].CompareTo(cameraControl.CameraControl.Resolution) == 0)
                {
                    index_to_select = index;
                }
            }

            // select current resolution
            if (index_to_select >= 0)
            {
                availableResolutions.SelectedIndex = index_to_select;
            }
        }

        private void availableDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.CurrentDevice = (string)availableDevices.SelectedValue;
            var camera_moniker = _CameraChoice.Devices[availableDevices.SelectedIndex].Mon;
            //availableDevices.ItemsSource = _CameraChoice.Devices;
            // Set selected camera to camera control with default resolution
            cameraControl.CameraControl.SetCamera(camera_moniker, null);
            FillResolutionList();
        }

        private void availableResolutions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!cameraControl.CameraControl.CameraCreated)
                return;

            int comboBoxResolutionIndex = availableResolutions.SelectedIndex;

            this.CurrentResolution = (string)availableResolutions.SelectedValue;
            if (comboBoxResolutionIndex < 0)
            {
                return;
            }
            ResolutionList resolutions = Camera.GetResolutionList(cameraControl.CameraControl.Moniker);

            if (resolutions == null)
                return;

            if (comboBoxResolutionIndex >= resolutions.Count)
                return; // throw

            if (0 == resolutions[comboBoxResolutionIndex].CompareTo(cameraControl.CameraControl.Resolution))
            {
                // this resolution is already selected
                return;
            }

            // Recreate camera
            cameraControl.CameraControl.SetCamera(cameraControl.CameraControl.Moniker, resolutions[comboBoxResolutionIndex]);
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (CameraActive) {
                var zoom = CurrentZoomFactor;
                var x = zoom * 25;
                var y = zoom * 25;
                var w = this.cameraControl.CameraControl.Resolution.Width - (50 * zoom);
                var h = this.cameraControl.CameraControl.Resolution.Height - (50 * zoom);
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(
                      (int)(x),
                      (int)(y),
                      (int)(w),
                      (int)(h)
                  );

                if (cameraControl.CameraControl.CameraCreated)
                    cameraControl.CameraControl.ZoomToRect(rect);
            }
          
        }




        
    }
}
