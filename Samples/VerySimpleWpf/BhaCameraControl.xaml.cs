﻿using Camera_NET;
using DirectShowLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VerySimpleWpf
{
    /// <summary>
    /// Interaction logic for BhaCameraControl.xaml
    /// </summary>
    public partial class BhaCameraControl : UserControl, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        CameraChoice _CameraChoice = new CameraChoice();

        public BhaCameraControl()
        {
            InitializeComponent();

            Application.Current.Exit += AppExitHandler;

            this.ZoomFactor = 1.0;

            this.SnapWithText = true;
            this.Alignment = "Left";
            this.Comment = "Hello";

        }

        private void AppExitHandler(object sender, ExitEventArgs e)
        {
            try
            {
                this.CameraControlWpf1.CameraControl.CloseCamera();
            }
            catch
            {
                //Do nothing, error silently
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property.Name == "CurrentDevice")
            {
                DsDevice newDevice = (DsDevice)e.NewValue;
                if (newDevice != null)
                {
                    this.CameraControlWpf1.CameraControl.SetCamera(newDevice.Mon, null);
                    CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                    //this.CameraControlWpf1.CameraControl.SetCamera(newDevice.Mon, null);
                }
                else
                {
                    this.CameraControlWpf1.CameraControl.SetCamera(null, null);
                }
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("AvailableResolutions"));
                }
                this.CurrentResolution = this.CameraControlWpf1.CameraControl.Resolution;
            }


            if (e.Property.Name == "CurrentResolution")
            {
                Resolution newResolution = (Resolution)e.NewValue;
                if (this.CameraControlWpf1.CameraControl.Resolution != newResolution)
                {
                    this.CameraControlWpf1.CameraControl.SetCamera(this.CurrentDevice.Mon, newResolution);
                    CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                    CameraControlWpf1.CameraControl.MixerEnabled = true;
                    this.ZoomFactor = 1.0;
                }
            }

            if (e.Property.Name == "ShowText")
            {
                CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                CameraControlWpf1.CameraControl.MixerEnabled = true;
            }

            if (e.Property.Name == "ZoomFactor")
            {
                if (this.CameraControlWpf1.CameraControl.CameraCreated)
                {
                    double newValue = (double)e.NewValue;
                    var zoom = newValue;

                    var resolution = this.CameraControlWpf1.CameraControl.Resolution;

                    var w = (resolution.Width / zoom);
                    var h = (resolution.Height / zoom);
                    var x = (resolution.Width - w) / 2.0;
                    var y = (resolution.Height - h) / 2.0;

                    System.Drawing.Rectangle rect = new System.Drawing.Rectangle(
                          (int)(x),
                          (int)(y),
                          (int)(w),
                          (int)(h)
                      );

                    this.CameraControlWpf1.CameraControl.ZoomToRect(rect);
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _CameraChoice.UpdateDeviceList();
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs("AvailableDevices"));
            }
            this.CurrentDevice = AvailableDevices.FirstOrDefault();
            
        }

        public List<DsDevice> AvailableDevices
        {
            get { return _CameraChoice.Devices; }
        }

        public List<Resolution> AvailableResolutions
        {
            get
            {
                if (this.CurrentDevice != null)
                {
                    ResolutionList resolutions = Camera.GetResolutionList(this.CurrentDevice.Mon);
                    return resolutions;
                }
                else
                {
                    return null;
                }

            }
        }

        public static readonly DependencyProperty CurrentDeviceProperty = DependencyProperty.Register("CurrentDevice", typeof(DsDevice), typeof(BhaCameraControl));
        public DsDevice CurrentDevice
        {
            get
            {
                return (DsDevice)this.GetValue(CurrentDeviceProperty);
            }
            set
            {
                this.SetValue(CurrentDeviceProperty, value);
            }
        }

        public static readonly DependencyProperty CurrentResolutionProperty = DependencyProperty.Register("CurrentResolution", typeof(Resolution), typeof(BhaCameraControl));
        public Resolution CurrentResolution
        {
            get
            {
                return (Resolution)this.GetValue(CurrentResolutionProperty);
            }
            set
            {
                this.SetValue(CurrentResolutionProperty, value);
            }
        }

        public static readonly DependencyProperty ZoomFactorProperty = DependencyProperty.Register("ZoomFactor", typeof(double), typeof(BhaCameraControl));
        public double ZoomFactor
        {
            get
            {
                return (double)this.GetValue(ZoomFactorProperty);
            }
            set
            {
                this.SetValue(ZoomFactorProperty, value);
            }
        }

        public static readonly DependencyProperty ShowTextProperty = DependencyProperty.Register("ShowText", typeof(string), typeof(BhaCameraControl));
        public string ShowText
        {
            get
            {
                return (string)this.GetValue(ShowTextProperty);
            }
            set
            {
                this.SetValue(ShowTextProperty, value);
            }
        }

        private bool _SnapWithText;
        public bool SnapWithText
        {
            set
            {
                _SnapWithText = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("SnapWithText"));
                }
            }
            get
            {
                return _SnapWithText;
            }
        }

        private string _Alignment;
        public string Alignment
        {
            get
            {
                return _Alignment;
            }

            set
            {
                _Alignment = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("Alignment"));
                }
            }
        }

        private string _Comment;
        public string Comment
        {
            get
            {
                return _Comment;
            }

            set
            {
                _Comment = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs("Comment"));
                }
            }
        }

        public byte[] GetSnapshot()
        {
            return null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
            CameraControlWpf1.CameraControl.MixerEnabled = true;
            this.Snapshot();
        }

        private void Snapshot()
        {
            if (!CameraControlWpf1.CameraControl.CameraCreated)
                return;
            

            Bitmap bitmap = null;
            try
            {
                if (this.SnapWithText == true)
                {
                    bitmap = CameraControlWpf1.CameraControl.SnapshotOutputImage();
                }
                else
                {
                    bitmap = CameraControlWpf1.CameraControl.SnapshotSourceImage();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error while getting a snapshot");
            }

            if (bitmap == null)
                return;
            bitmap.Save("C:\\temp\\snapshot.jpg");

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (!CameraControlWpf1.CameraControl.CameraCreated)
                return;

            Bitmap bitmap = CameraControlWpf1.CameraControl.SnapshotOutputImage();

            if (bitmap == null)
                return;
            bitmap.Save("C:\\temp\\snapshot.jpg");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
            CameraControlWpf1.CameraControl.MixerEnabled = true;
        }

        private Bitmap GenerateColorKeyBitmap(bool useAntiAlias)
        {
            if (CameraControlWpf1.CameraControl.CameraCreated)
            {
                int w = CameraControlWpf1.CameraControl.Resolution.Width;
                int h = CameraControlWpf1.CameraControl.Resolution.Height;

                if (w <= 0 || h <= 0)
                    return null;

                // Create RGB bitmap
                Bitmap bmp = new Bitmap(w, h);
                Graphics g = Graphics.FromImage(bmp);

               
                // configure antialiased drawing or not
                if (useAntiAlias)
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;
                    g.TextRenderingHint = TextRenderingHint.AntiAlias;
                }
                else
                {
                    g.SmoothingMode = SmoothingMode.None;
                    g.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;
                }
                bmp.Save("C:\\TEmp\\t1.bmp");
                // Clear the bitmap with the color key
                g.Clear(CameraControlWpf1.CameraControl.GDIColorKey);
                bmp.Save("C:\\TEmp\\t2.bmp");

                // Draw text logo for example
                {
                    Font font = new Font("Tahoma", 16);
                    System.Drawing.Brush textColorKeyed = new SolidBrush(System.Drawing.Color.Yellow);
                    var date = DateTime.Now.ToString();

                    //g.DrawString(date, font, textColorKeyed, 4, h - 30);
                    //g.DrawString()
                    var dateSize = g.MeasureString(date, font);

                    //if (dateSize.Width > w)
                    //{
                        
                    //    string[] words = date.Split(' ');
                    //    StringBuilder sb = new StringBuilder();
                    //    int currLength = 0;
                    //    float wordsSize = 0;
                    //    foreach (string word in words)
                    //    {
                    //        wordsSize = wordsSize + g.MeasureString(word, font).Width;
                    //        if (currLength + wordsSize + 1 < w) // +1 accounts for adding a space
                    //        {
                    //            sb.AppendFormat(" {0}", word);
                    //            currLength = (int)(sb.Length % w);
                    //        }
                    //        else
                    //        {
                    //            sb.AppendFormat("{0}{1}", Environment.NewLine, word);
                    //            currLength = 0;
                    //        }
                    //    }
                        
                    //    date = sb.ToString();
                    //}

                    if (dateSize.Width > w)
                    {
                        font= new Font("Tahoma", 7);
                    }
                    else
                    {
                        font = new Font("Tahoma", 16);
                    }
                    dateSize = g.MeasureString(date, font);
                    if (this.Alignment == "Left")
                    {
                        g.DrawString(date, font, textColorKeyed, 4, h- dateSize.Height);
                    }
                    else if (this.Alignment == "Center")
                    {
                        g.DrawString(date, font, textColorKeyed, (w / 2) - (dateSize.Width / 2), h - dateSize.Height);
                    }
                    else if (this.Alignment == "Right")
                        g.DrawString(date, font, textColorKeyed, w - dateSize.Width, h - dateSize.Height);

                    //string.Format("Hello,{0}How are you?{0}I'm fine!", Environment.NewLine);


                    var commentSize = g.MeasureString(this.Comment, font);

                    //if the text ovwrflows the width//

                    //if (commentSize.Width > w)
                    //{

                    //    string[] words = this.Comment.Split(' ');
                    //    StringBuilder sb = new StringBuilder();
                    //    int currLength = 0;
                    //    float wordsSize = 0;
                    //    foreach (string word in words)
                    //    {
                    //        wordsSize = wordsSize + g.MeasureString(word, font).Width;
                    //        if (currLength + wordsSize + 1 < w) // +1 accounts for adding a space
                    //        {
                    //            sb.AppendFormat(" {0}", word);
                    //            currLength = (int)(sb.Length % w);
                    //        }
                    //        else
                    //        {
                    //            sb.AppendFormat("{0}{1}", Environment.NewLine, word);
                    //            currLength = 0;
                    //        }
                    //    }

                    //    this.Comment = sb.ToString();
                    //}


                    if (this.Alignment == "Left")
                    {
                        g.DrawString(this.Comment, font, textColorKeyed, 4, 0);
                    }
                    else if (this.Alignment == "Center")
                    {
                        g.DrawString(this.Comment, font, textColorKeyed, (w / 2) - (commentSize.Width / 2), 0);
                    }
                    else
                        g.DrawString(this.Comment, font, textColorKeyed, w - commentSize.Width, 0);
                }

                // dispose Graphics
                g.Dispose();

                // return the bitmap

                bmp.Save("c:\\Temp\\text.bmp");

                return bmp;
            }
            return null;
        }       

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.SnapWithText = true;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.SnapWithText = false;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            this.Alignment = (string)rb.Content;

            CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
            CameraControlWpf1.CameraControl.MixerEnabled = true;
        }

        private void CameraControlWpf1_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (CameraControlWpf1 != null)
            {
                CameraControlWpf1.CameraControl.OverlayBitmap = GenerateColorKeyBitmap(false);
                CameraControlWpf1.CameraControl.MixerEnabled = true;
            }
        }
    }
}
